$(document).ready(function(){
   
    // determine content area height
	/*
	var $theContent;
	var theContentHeight;
	var nonContentHeight = $('#header').outerHeight() + $('#footer').outerHeight();
	var $theContent = $('#content');
	var innerContentHeight = $(window).height() - nonContentHeight;		
		
	function getContentHeight(){			
		if ( $theContent.height() < innerContentHeight ) theContentHeight = innerContentHeight
	}
	getContentHeight();	
	*/	
	
	// preload image
	function preLoadImage(imgSrc){
		var preLoadImg = new Image();
		$(preLoadImg)
			.load()
			.attr('src', imgSrc);
	}
   
   // resize modal overlay to cover only content area
   function modalLoaded(){	  
		//$('#cboxOverlay').height(theContentHeight);	
   }   
   
   function modalComplete(){    		 
	   
	   // naming convention for over state
		var overName = '-over';
		
		// build over src
		var imgUp = $('#gate-container input.inputBtn').attr('src');
		var imgExt = imgUp.substring(imgUp.length-4, imgUp.length);
		var imgOver = (imgUp.substring(0,imgUp.length-4))+overName+imgExt;
		
		// preload over image
		preLoadImage(imgOver);
		
		$('#gate-container input.inputBtn').hover(
		  function () {
			$(this).attr('src',imgOver);
		  }, 
		  function () {
			$(this).attr('src',imgUp);
		  }		  
		);
		
		
		// Form default value show/hide
		$(function() {
		  $('input[type="text"]:not(".noblur")').add('textarea')	  
		  .focus(function() {
			var $this = $(this);
			
			if (!$this.data('default')) {
			  $this.data('default', $this.val());
			}
			
			if ($this.val() == $this.data('default')) {
			  $this
				.val('')
				.css({color:'#333'});
			}
		  })	  
		  .blur(function() {
			var $this = $(this);
			
			if ($this.val() == '') {
			  $(this)
				.val($this.data('default'))
				.css({color:'#999'});
			}
		  })	  
		});
		
		
   }
   
    var value = $.readCookie('diageoagecheck');
    if( value ) return false;
    else {		
		// hide content and nav
		$('#nav, #search_form, #content').css({opacity:0});		
		$('#nav, #search_form').hide();	
		$('#content > *').hide();
		
		$(".agegate").colorbox({
			onLoad: modalLoaded,
			onComplete: modalComplete,
			scrolling: false,
			open: true,
			close: '',
			escKey: false,
			overlayClose: false,
			opacity: 1});

	}
});
    
    
/* jQuery CooQuery Plugin v2 (minified) - http://cooquery.lenonmarcel.com.br/
Copyright 2009, 2010 Lenon Marcel
Dual licensed under the MIT and GPL licenses.
http://www.opensource.org/licenses/mit-license.php
http://www.gnu.org/licenses/gpl.html */
(function($){$.setCookie=function(name,value,options){if(typeof name==='undefined'||typeof value==='undefined')
return false;var str=name+'='+encodeURIComponent(value);if(options.domain)str+='; domain='+options.domain;if(options.path)str+='; path='+options.path;if(options.duration){var date=new Date();date.setTime(date.getTime()+options.duration*24*60*60*1000);str+='; expires='+date.toGMTString();}
if(options.secure)str+='; secure';return document.cookie=str;};$.delCookie=function(name){return $.setCookie(name,'',{duration:-1});};$.readCookie=function(name){var value=document.cookie.match('(?:^|;)\\s*'+name.replace(/([-.*+?^${}()|[\]\/\\])/g,'\\$1')+'=([^;]*)');return(value)?decodeURIComponent(value[1]):null;};$.CooQueryVersion='v 2.0';})(jQuery);    