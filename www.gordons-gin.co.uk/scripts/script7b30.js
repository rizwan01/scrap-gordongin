if (top.location != location) {
    top.location.href = document.location.href;
}
    
var isIE6 = false;
var isIE7 = false;
var isIE = false;
if ($.browser.msie && $.browser.version.substr(0, 1) < 7) isIE6 = true;
if ($.browser.msie && $.browser.version.substr(0, 1) == 7) isIE7 = true;
if ($.browser.msie && $.browser.version.substr(0, 1) < 9) isIE = true; // includues IE8 and below
var headfootHeight = $('#header').outerHeight() + $('#footer').outerHeight();

function resizeContainer() {
    $('#container').css({ 'overflow-y': 'hidden' });

    var containerHeight = $('#content').height() + headfootHeight;
    if (containerHeight >= $(window).height()) {
        $('#container').css('height', containerHeight);
    } else {
        $('#container').css('height', $(window).height());
    }

}

 // Cookie for Inspiration Filter list
     function setCookie(c_name, value, exdays) {
           var exdate = new Date();
           exdate.setDate(exdate.getDate() + exdays);
           var c_value = escape(value) + ((exdays == null) ? "" : "; path=/; expires=" + exdate.toUTCString());
           document.cookie = c_name + "=" + c_value;
       }
       
      function setURL(catName) {
           setCookie("incat", catName, 365);
       }

       function setURLOnload() {
           var URLlink = window.location.href;
           var exists = URLlink.indexOf('#');
           if (exists != -1) {
               var catName = URLlink.substring(exists + 2, URLlink.length - 1)
               setCookie("incat", catName, 365);
           }
       } 


// let's set the #content div to fill the bg image area to so we can apply moire			
function setContentHeight() {
    var $content = $('#content');
    var innerHeight = $(window).height() - headfootHeight;
    $content.height('auto');

    if (!$content.hasClass('flash')) {
        if ($content.height() < innerHeight) {		

            $content.height(innerHeight);
            $('#cboxOverlay').height(innerHeight);
        }
        $content.addClass('moire');
    } else { // if flash content
        $content.height(innerHeight);
        $('#cboxOverlay').height(innerHeight);
    }

    // dynamically adjust the Y positioning of content panels		
    function centerContentPanel($el) {
        recipeH = $el.innerHeight() / 2;
        contentH = ($content.innerHeight() - 90) / 2;
        recipePos = (contentH - recipeH);
        if (recipePos < 40) recipePos = 40;
        $el.css({ 'margin-top': recipePos });

    }
    
   
    if (isIE6 || isIE7) {
        centerContentPanel($('#recipe, #event'));
    } else {
        centerContentPanel($('#recipe, #event, #registration'));
    }
}


function checkParemeterExists(parameter) {
    //Get Query String from url
    fullQString = window.location.search.substring(1);

    paramCount = 0;
    queryStringComplete = "?";

    if (fullQString.length > 0) {
        //Split Query String into separate parameters
        paramArray = fullQString.split("&");

        //Loop through params, check if parameter exists.  
        for (i = 0; i < paramArray.length; i++) {
            currentParameter = paramArray[i].split("=");
            if (currentParameter[0] == parameter) //Parameter already exists in current url
            {
                return true;
            }
        }
    }

    return false;
}

$(function () {

    $('#slide_content .slide .panels, #slide_content .slide .floater').hide();

    // Gateway Background image start here

    //$("#bg_container").html($("#bg_container img:first"));

    // Bypass the agegateway
    if (checkParemeterExists("allowAccess") == true && $.readCookie('diageoagecheck') == null) {
        createCookie('diageoagecheck', 'true', 0);
        location.reload(true);
    }

    if ($.readCookie('diageoagecheck') == null) {
        $('#bg_container').cycle('pause');
        $("#gatewayBGImage").show();
        $("#bg_container").hide();
        $("#container").css('position', 'static');
        $("body, html").css("overflow", "hidden");
    }
    else {
        $("#bg_container").show();
        $("#gatewayBGImage").hide();
    }

    $("#gatewayBGImage").css({ 'height': $(window).height(), 'width': $(window).width() });

    $(window).resize(function () {
        $("#gatewayBGImage").css({ 'height': $(window).height(), 'width': $(window).width() });
    });

    // Gateway Background image ends here


    // open links in new window
    $("a[target='_blank']").addClass('external');
    $("a[rel='external']").attr("target", "_blank");

    // print page
    $(".page_functions a.print").click(function () {
        window.print();
        return false;
    });

    // remove ClearType
    function fixType($el) {
        $el.each(function () {
            this.style.removeAttribute('filter');
        });
    }

    // preload image
    function preLoadImg(imgSrc) {
        var preLoadImg = new Image();
        $(preLoadImg)
			.load()
			.attr('src', imgSrc);
    }

    // Form default value show/hide
    $(function () {
        $('input[type="text"]:not(".noblur")').add('textarea')
	  .focus(function () {
	      var $this = $(this);


	      if (!$this.data('default')) {

	          if ($this[0].id.match(/_tbxMobile/)) {
	              if ($this.val() == 'Phone number' || $.trim($this.val()) == '')
	                  $this.data('default', 'Phone number');
	          }
	          else if ($this[0].id.match(/_tbxAddress2/)) {
	              if ($this.val() == 'Address 2' || $.trim($this.val()) == '')
	                  $this.data('default', 'Address 2');
	          }
	          else {
	              $this.data('default', $this.val());
	          }
	      }

	      if ($this.val() == $this.data('default')) {

	          $this
	          .val('')
			.css({ color: '#333' });
	      }
	  })
	  .blur(function () {
	      var $this = $(this);

	      if ($this.val() == '') {
	          $(this)
		  	.val($this.data('default'))
			.css({ color: '#999' });
	      }
	  })
    });


    /* Background image loader
    ********************************************/


    // load single image	
    (function loadBg() {
        var $bgImg = $('#current_bg');
        var imgSrc = $bgImg.attr('src');

        // turning it off and on again fixes everything. always.
        $bgImg.attr('src', '');
        $bgImg.attr('src', imgSrc);
        $bgImg.hide();

        // create image object
        var loadImg = new Image();

        // fade in background image on image load		
        $(loadImg)
        			.load(function () {
        			    $('#current_bg').fadeIn(1200, function () {
        			        if ($('body .slideshow').length > 0) {
        			            //alert('startslide show');
        			            initSlideshow();
        			        }
        			        //alert('no slideshow');
        			    });
        			})
        			.error(function () {
        			    $('#current_bg').html('error loading image');
        			})
        			.attr('src', imgSrc);
    })();


    /* Slideshow
    ********************************************/
    function initSlideshow() {


        // position slideshow content relative to browser height
        function positionSlideshowElements() {
            var windowHeight = $(window).height();
            if (windowHeight < 715) {

                var rangeYPercantage = (windowHeight - 615) / 100;
                if (rangeYPercantage <= 0) rangeYPercantage = 0;

                function calculateHeight(el, minYPos, maxYPos) {
                    var rangeY = maxYPos - minYPos;
                    var newYPos = (rangeY * rangeYPercantage) + minYPos;
                    el.css({ top: newYPos + '%' });
                }

                calculateHeight($('#slide_content .slide .panels'), 50, 60); // minYPos & maxYPos specified in percentage
                calculateHeight($('#slide_controls a'), 35, 45);

                // any special elements (.floater)					
                calculateHeight($('#slide_content .homeslide_2 .floater'), 13, 22);

            } else { // if window height more than 715, let's remove any special js positioning adjustments				

                $('#slide_content .slide .panels').css({ top: '' });
                $('#slide_controls a').css({ top: '' });

                // special elements (.floater)
                $('#slide_content .homeslide_2 .floater').css({ top: '22%' });
            }
        }

        positionSlideshowElements();
        $(window).resize(function () { positionSlideshowElements(); });

        var firstSlide = true;
        $('#slide_controls').show();
        $('#slide_content .slide').show();
        //$('#slide_content .slide .panels, #slide_content .slide .floater').hide();

        if ($("#slide_pager").length == 0)
            $('#slide_content .slide .panels, #slide_content .slide .floater').fadeIn(1200);

        if (isIE) {

            function onBeforeIE() {
                if (!firstSlide) {
                    $('#slide_content .slide .panels, #slide_content .slide .floater').hide();
                } else {
                    $('#slide_content .slide:eq(0) .panels, #slide_content .slide:eq(0) .floater').fadeIn(1200, function () {
                        this.style.removeAttribute('filter');
                        $(this).find('h1').animate({ opacity: 99.9 }, 1).fadeIn(1);
                    });
                }
                firstSlide = false;
            }

            function onAfterIE() {
                // get current slide number from pager		
                var id = $("#slide_pager .activeSlide").index('#slide_pager a');
                if (id < 0) id = 0;

                $('#slide_content .slide:eq(' + id + ') .floater').show();
                $('#slide_content .slide:eq(' + id + ') .panels').fadeIn(1200, function () {
                    this.style.removeAttribute('filter');
                    $(this).find('h1').animate({ opacity: 99.9 }, 1).fadeIn(1);
                });
            }

            function onStart() {
                // dummy
            }

            $('#bg_container').cycle({
                fx: 'none',
                speed: 1200,   // fade effect speed in milliseconds
                timeout: 15000, // slideshow speed in milliseconds 
                next: '#next_slide',
                prev: '#prev_slide',
                before: onBeforeIE,
                startSlide: onAfterIE,
                pager: '#slide_pager'
            });

        } else { // not IE

            function onBefore() {
                if (!firstSlide) {
                    $('#slide_content .slide .panels, #slide_content .slide .floater').fadeOut(600);
                } else {
                    $('#slide_content .slide:eq(0) .panels, #slide_content .slide:eq(0) .floater').fadeIn(1200, function () {
                        if (isIE) { // IE rendering bug fixes
                            this.style.removeAttribute('filter');
                            $(this).find('h1').animate({ opacity: 99.9 }, 1).fadeIn(1);
                        }
                    });
                }
                firstSlide = false;
            }

            function onAfter() {
                // get current slide number from pager		
                var id = $("#slide_pager .activeSlide").index('#slide_pager a');
                if (id < 0) id = 0;
                $('#slide_content .slide:eq(' + id + ') .panels, #slide_content .slide:eq(' + id + ') .floater').fadeIn(1200, function () {
                    if (isIE) { // IE rendering bug fixes
                        this.style.removeAttribute('filter');
                        $(this).find('h1').animate({ opacity: 99.9 }, 1).fadeIn(1);
                    }
                });

            }

            function onStart() {
                // dummy
            }

            $('#bg_container').cycle({
                fx: 'fade',
                speed: 1200,   // fade effect speed in milliseconds
                timeout: 15000, // slideshow speed in milliseconds 
                next: '#next_slide',
                prev: '#prev_slide',
                before: onBefore,
                startSlide: onAfter,
                pager: '#slide_pager'
            });
        }

    }


    /* Background image layout enhancements 
    ********************************************/

    function scrollHeader() {
        scrollAmount = $(window).scrollTop();
        $('#header').css({
            'top': scrollAmount
        });
        $('#current_bg').css({
            'top': scrollAmount + 60
        });
        $('#filters').css({
            'top': (scrollAmount + 110)
        });
    }


    // ie6 layout shims
    if (isIE6) {
        resizeContainer();
        $(window).resize(function () { resizeContainer(); });
        $(window).scroll(function () { scrollHeader(); });

    }

    setContentHeight();
    $(window).resize(function () {
        setContentHeight();
    });


    /* Pagination
    ********************************************/
    function paginate(wrapID, filterId) {

        setContentHeight();

        // if no filterId passed, assume all
        if (!filterId) filterId = 'result';

        var itemsRange = 10;
        var itemsHidden = $(wrapID + ' .' + filterId).length - itemsRange;
        var itemsShown;
        var paginateText = $(wrapID + ' > h1.pagination_text').text();

        if (itemsHidden > 0) {

            // hide items and attach pagination controls
            $(wrapID + ' .' + filterId + ':gt(' + (itemsRange - 1) + ')').hide();

            $(wrapID).append('<div class="pagination"><a class="view_more">View <span>10</span> more ' + paginateText + '</a><a class="view_all">View all</a></div>');
            if (itemsHidden < itemsRange) $(wrapID + ' .pagination a.view_more span').text(itemsHidden);

            // 'view more' button click
            $(wrapID + ' .pagination a.view_more').click(function () {
                itemsShown = $(wrapID + ' .' + filterId + ':visible').length;
                commentCap = itemsShown + itemsRange;

                // fade-in next set of items
                $(wrapID + ' .' + filterId + ':lt(' + commentCap + '):gt(' + (itemsShown - 1) + ')').fadeIn(500, function () {
                    itemsHidden = $(wrapID + ' .' + filterId + ':hidden').length;

                    // if hidden items are less than range, update pagination text
                    if (itemsHidden < itemsRange) $(wrapID + ' .pagination a.view_more span').text(itemsHidden);

                    // if no more items, remove pagination controls
                    if (itemsHidden < 1) $(wrapID + ' .pagination').remove();
                });
                if (isIE6) resizeContainer();
                return false;
            });

            // 'view all' button click
            $(wrapID + ' .pagination a.view_all').click(function () {
                $(wrapID + ' .' + filterId + ':hidden').fadeIn(500);
                $(this).parent('.pagination').remove();
                if (isIE6) resizeContainer();
                return false;
            });
        }

        if (isIE6) resizeContainer();

    }

    /* Paginate search results
    ********************************************/
    if ($('#search_results').length > 0) {
        paginate('#search_results');
    };


    /* Filter results
    ********************************************/
    if ($('#results').length > 0) {

        function checkHashLocation() {

            // Get hash location and show relevant results
            var hashLocation = window.location.hash;
            hashLocation = hashLocation.substring(1, hashLocation.length);
            var hashStripped = hashLocation.replace(new RegExp("/", "g"), '');

            if (hashStripped != "" && hashStripped != "all") {
                // show hashed items
                $('#results .' + hashStripped).fadeIn(function () {
                    if (isIE) this.style.removeAttribute('filter');
                });
                showActiveMenuItem(hashLocation);
                paginate('#results', hashStripped);
            } else {
                // else show all items		
                $('#results .result').fadeIn(function () {
                    if (isIE) this.style.removeAttribute('filter');
                });
                window.location.hash = '/all/';
                paginate('#results');
            }
        }


        // animate panels in on click
        $('#filters li a').click(function () {

            setContentHeight();
            var aniSpeed = 450;

            // reset pagination
            $('#results .pagination').remove();

            hashValue = $(this).attr('href');
            hashValue = hashValue.substring(1, hashValue.length);
            var hashStripped = hashValue.replace(new RegExp("/", "g"), '');


            // add active class to filters and animate out any that were previously active
            $('#filters li a').removeClass('active');
            $(this).addClass('active');
            $('#filters li a:not(".active")').animate({ 'padding-left': 7 }, 160);

            if (hashStripped == 'all') {

                var iterationAll = $('#results .result').length;
                var i = 0;

                $('#results .result')
					.stop()
					.animate({
					    'height': '130px',
					    'opacity': 1,
					    'marginBottom': '10px'
					}, aniSpeed, function () {
					    i++;
					    if (i == iterationAll) {
					        paginate('#results');
					        if (isIE) fixType($('#results .result:visible'));
					    }
					});

                window.location.hash = this.hash;

            } else {

                // filter out items
                function filterOut() {
                    $('#results .result:not(".' + hashStripped + '")')
            .animate({
                'height': 0,
                'opacity': 0,
                'marginBottom': '0'
            }, aniSpeed);
                }

                // filter in items                
                function filterIn() {
                    var iterationSelected = $('#results .' + hashStripped).length;
                    var j = 0;

                    $('#results .' + hashStripped)
        .stop()
        .animate({
            'height': '130px',
            'opacity': 1,
            'marginBottom': '10px'
        }, aniSpeed, function () {
            j++;
            if (j == iterationSelected) {
                paginate('#results', hashStripped);
                if (isIE) fixType($('#results .result:visible'));
            }
        });

                }

                filterOut();
                filterIn();
            }


            prevHash = window.location.hash = this.hash;


            if (isIE6) scrollHeader();
            return false;

        });


        function showActiveMenuItem(menuItemId) {
            $('#filters > li > a')
				.removeClass('active')
				.removeAttr('style')
				.each(function () {
				    matchItemID = ($(this).attr('href')).substring(1, $(this).attr('href').length);
				    if (menuItemId == matchItemID) $(this).addClass('active');
				});
        }


        // Listen for hash changes - allows use of back/forward button			
        var prevHash;
        $(window).hashchange(function () {
            var hashLoc = location.hash;
            // if new hash is different to hash recorded on last filter click, apply sorting filter
            if (hashLoc != prevHash) {
                if (hashLoc == '') {
                    //$('#filters li a[href="#/all/"]').click();	
                    history.go(-1);
                } else {
                    $('#filters li a[href="' + hashLoc + '"]').click();
                }
            }
        });


        /* INITIATE FILTERS 
        ** let's kick off by hiding all results & then checking URL for hash value
        *******************************/
        $('#results .result').hide();
        checkHashLocation();

    }

    /** Indent filter labels on hover
    *************************************/
    function indentNav(el, normalPad, activePad) {
        $el = el.find('li a');
        $el.hover(function () {
            $(this).animate({ 'padding-left': activePad }, 160);
        }, function () {
            if (!($(this).hasClass("active"))) {
                $(this).animate({ 'padding-left': normalPad }, 160);
            }
        });
    }

    indentNav($('#filters'), 7, 12);
    indentNav($('#default'), 11, 16);
    indentNav($('#backnav'), 11, 16);



    /** Button rollovers
    *************************************/
    function inputBtnRollover($el) {

        if ($el.length > 0) {

            // naming convention for over state
            var overName = '-over';

            // build over src
            var imgUp = $el.attr('src');
            var imgExt = imgUp.substring(imgUp.length - 4, imgUp.length);
            var imgOver = (imgUp.substring(0, imgUp.length - 4)) + overName + imgExt;


            // preload over img
            preLoadImg(imgOver);

            $el.hover(
				  function () {
				      $(this).attr('src', imgOver);
				  },
				  function () {
				      $(this).attr('src', imgUp);
				  }
				);
        }

    }

    function buttonRollover($el, idx) {

        // if image index specified use that, else default to 1st image
        if (idx) var imgIdx = idx
        else var imgIdx = 0;

        if ($el.length > 0) {

            $img = $el.find('img').eq(imgIdx);

            // naming convention for over state
            var overName = '-over';

            // build over src
            var imgUp = $img.attr('src');
            var imgExt = imgUp.substring(imgUp.length - 4, imgUp.length);
            var imgOver = (imgUp.substring(0, imgUp.length - 4)) + overName + imgExt;


            // preload over img
            preLoadImg(imgOver);

            $el.hover(
				  function () {
				      $(this).find('img').eq(imgIdx).attr('src', imgOver);
				  },
				  function () {
				      $(this).find('img').eq(imgIdx).attr('src', imgUp);
				  }
				);


        }

    }

    // input buttons
    inputBtnRollover($('#search_form input.searchBtn'));
    inputBtnRollover($('#searchForm input.inputBtn'));
    inputBtnRollover($('#content #sf3 input.submitbutton'));


    // images inside rollover element
    buttonRollover($('#content a.teaser'), 1);
    buttonRollover($('.calendar #rhs .lng_content a.panel'), 0);
    buttonRollover($('#content #sf1 a.nextbutton'));
    buttonRollover($('#content #sf2 a.nextbutton'));
    buttonRollover($('#content #ctl00_ctl00_ctl00_ContentPlaceHolderDefault_MainContent_registration_4_pnlJoin a.nextbutton'));
    buttonRollover($('#content #ctl00_ctl00_ctl00_ContentPlaceHolderDefault_MainContent_registration_4_pnlRegistration1 a.nextbutton'));
    inputBtnRollover($('#content #ctl00_ctl00_ctl00_ContentPlaceHolderDefault_MainContent_registration_4_pnlRegistration2 input.submitbutton'));
    buttonRollover($('#backnav li a'));

    // homeslide rollovers	
    if ($('#slide_content').length > 0) {

        var floaterSrc;
        var floaterSrcOver;

        // preload rollovers
        $('#slide_content .slide a.floater').each(function () {
            floaterSrcOver = $(this).attr('rel');
            preLoadImg(floaterSrcOver);
        });

        $('#slide_content .slide a.floater').hover(
			function () {
			    if ($(this).attr('rel').length > 0) {
			        floaterSrc = $(this).find('img').attr('src');
			        $(this).find('img').attr('src', $(this).attr('rel'));
			    }
			},
			function () {
			    if ($(this).attr('rel').length > 0) {
			        $(this).find('img').attr('src', floaterSrc);
			    }
			});


    }


    /** Video lightbox
    *************************************/
    if ($('#content a.lightbox').length > 0) {

        function vidModalComplete() {
            $('#cboxClose').click(function () {
                $.colorbox.close();
            });

            $('#video-container .transcript_link a.text_link').click(function () {
                transcriptID = $(this).attr('href');
                transcriptID = transcriptID.substr(transcriptID.indexOf('#'));

                // hide video and link
                $('#lb_video_embed iframe, #video-container .transcript_link').hide();
                // show transcript
                $(transcriptID).show();
                $('#video-container .video_link a.text_link').click(function () {
                    $('#lb_video_transcript').hide();
                    $('#lb_video_embed iframe, #video-container .transcript_link').show();
                    return false;
                });
                return false;
            });
        }

        $("#content a.lightbox").colorbox({
            overlayClose: true,
            open: false,
            close: '',
            opacity: 0.4,
            innerWidth: 720,
            innerHeight: 432,
            scrolling: false,
            onComplete: vidModalComplete
        });
    }

    /** Video transcript
    *************************************/
    $('#rhs .transcript_link a.text_link').click(function () {
        transcriptID = $(this).attr('href');
        transcriptID = transcriptID.substr(transcriptID.indexOf('#'));
        // hide video and link
        $('#video_embed iframe, #rhs .transcript_link').hide();
        // show transcript
        $(transcriptID).show();
        $('#rhs .video_link a.text_link').click(function () {
            $('#video_transcript').hide();
            $('#video_embed iframe, #rhs .transcript_link').show();
            return false;
        });
        return false;
    });

});